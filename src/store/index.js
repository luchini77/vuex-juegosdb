import { createStore } from 'vuex'

export default createStore({
  state: {
    juegos: [],
    juego: [],
  },
  getters: {
  },
  mutations: {
    setJuegos(state, payload){
      state.juegos = payload
    }, 

    setJuego(state, payload){
      state.juego = payload
    }
  },
  actions: {
    async getJuegos({commit}){
      try {
        const res = await fetch('https://www.freetogame.com/api/games')
        const data = await res.json()

        commit('setJuegos', data)
      } catch (error) {
        console.log(error)
      }
    },

    async getJuego({commit}, id){
      try {
        const res = await fetch(`https://www.freetogame.com/api/game?id=${id}`)
        const data = await res.json()

        commit('setJuego', data)
      } catch (error) {
        console.log(error)
      }
    }
  },
  modules: {
  }
})
