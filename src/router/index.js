import { createRouter, createWebHistory } from 'vue-router'
import JuegosAll from '../views/JuegosAll.vue'

const routes = [
  {
    path: '/',
    name: 'juegos',
    component: JuegosAll
  },
  {
    path: '/detalles/:id',
    name: 'detalles',
    component: () => import('../views/DetallesJuego.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
